<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package LMH_new
 */

if ( ! function_exists( 'lmh_new_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function lmh_new_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s"><span>%2$s</span><span>%3$s</span></time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published updated" datetime="%1$s"><span>%2$s</span><span>%3$s</span></time><time class="updated" datetime="%4$s"><span>%5$s</span><span>%6$s</span></time>';
		}

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date("M") ),
			esc_html( get_the_date("d") ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date("M") ),
			esc_html( get_the_modified_date("d") )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( '%s', 'post date', 'lmh-new' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="meta posted-on">' . $posted_on . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	}
endif;

if ( ! function_exists( 'lmh_new_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function lmh_new_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( '%s', 'post author', 'lmh-new' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="meta byline"> ' . $byline . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link"><i class="icofont-speech-comments"></i>';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( '0 <span class="screen-reader-text">Comments, Leave a Comment on %s</span>', 'lmh-new' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				)
			);
			echo '</span>';
		}
	}
endif;

if ( ! function_exists( 'lmh_new_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function lmh_new_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			?><div class="d-flex justify-content-between  align-items-center"><div class="post-taxonomies"><?php
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'lmh-new' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links d-block">' . esc_html__( '%1$s', 'lmh-new' ) . '</span>', $categories_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'lmh-new' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( '%1$s', 'lmh-new' ) . '</span>', $tags_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
			?></div><?php
			add_shared_socialmedia_icon();
			?></div><?php
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'lmh-new' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'lmh_new_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function lmh_new_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

			<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
				<?php
					the_post_thumbnail(
						'post-thumbnail',
						array(
							'alt' => the_title_attribute(
								array(
									'echo' => false,
								)
							),
						)
					);
				?>
			</a>

			<?php
		endif; // End is_singular().
	}
endif;

if ( ! function_exists( 'wp_body_open' ) ) :
	/**
	 * Shim for sites older than 5.2.
	 *
	 * @link https://core.trac.wordpress.org/ticket/12563
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
endif;

function add_shared_socialmedia_icon(){
	//global $post;
	$content = lhm_string_to_link(get_the_content(),50);
	?>
<div id="share-buttons">

    <!-- Buffer -->
    <!--<a href="https://bufferapp.com/add?url=<?php echo esc_url( get_the_permalink() ) ?>&amp;text=<?php echo get_the_title(); ?>" target="_blank">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/buffer.png" alt="Buffer" />
    </a>-->
    
    <!-- Digg -->
    <!--<a href="http://www.digg.com/submit?url=<?php echo esc_url( get_the_permalink() ) ?>" target="_blank">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/diggit.png" alt="Digg" />
    </a>-->
    
    <!-- LinkedIn -->
    <!--<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo esc_url( get_the_permalink() ) ?>" target="_blank">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/linkedin.png" alt="LinkedIn" />
    </a>-->
    
    <!-- Pinterest -->
    <!--<a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/pinterest.png" alt="Pinterest" />
    </a>
    -->
    <!-- Print -->
    <!--<a href="javascript:;" onclick="window.print()">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/print.png" alt="Print" />
    </a>
    -->
    <!-- Reddit -->
    <!--<a href="http://reddit.com/submit?url=<?php echo esc_url( get_the_permalink() ) ?>&amp;title=<?php echo get_the_title(); ?>" target="_blank">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/reddit.png" alt="Reddit" />
    </a>
    -->
    <!-- StumbleUpon-->
    <!--<a href="http://www.stumbleupon.com/submit?url=<?php echo esc_url( get_the_permalink() ) ?>&amp;title=<?php echo get_the_title(); ?>" target="_blank">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/stumbleupon.png" alt="StumbleUpon" />
    </a>
    -->
    <!-- Tumblr-->
    <!--<a href="http://www.tumblr.com/share/link?url=<?php echo esc_url( get_the_permalink() ) ?>&amp;title=<?php echo get_the_title(); ?>" target="_blank">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/tumblr.png" alt="Tumblr" />
    </a>
     -->
    
    <!-- VK -->
    <!--<a href="http://vkontakte.ru/share.php?url=<?php echo esc_url( get_the_permalink() ) ?>" target="_blank">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/vk.png" alt="VK" />
    </a>
    -->
    <!-- Yummly -->
    <!--<a href="http://www.yummly.com/urb/verify?url=<?php echo esc_url( get_the_permalink() ) ?>&amp;title=<?php echo get_the_title(); ?>" target="_blank">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/yummly.png" alt="Yummly" />
    </a>
	-->
	<!-- Facebook -->
    <a href="http://www.facebook.com/sharer.php?u=<?php echo esc_url( get_the_permalink() ) ?>" target="_blank">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/facebook.png" alt="Facebook" />
    </a>
	
	<!-- Twitter -->
    <a href="https://twitter.com/share?url=<?php echo esc_url( get_the_permalink() ) ?>&amp;text=<?php echo $content ?>&amp;hashtags=<?php bloginfo( 'name' ); ?>" target="_blank">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/twitter.png" alt="Twitter" />
    </a>

	<!-- Email -->
    <a href="mailto:?Subject=<?php echo get_the_title(); ?>&amp;<?php echo $content ?><?php echo esc_url( get_the_permalink() ) ?>">
        <img src="<?php echo get_template_directory_uri()."/assets/"; ?>images/somacro/email.png" alt="Email" />
    </a>
</div>
	<?php
}