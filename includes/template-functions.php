<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package LMH_new
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function lmh_new_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'lmh_new_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function lmh_new_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'lmh_new_pingback_header' );

function lhm_string_to_link($content, $maxlenght){
	$content = wp_strip_all_tags($content);
	$content = substr($content, 0, $maxlenght );
	$content = str_replace(" ", "%20", $content);

	return $content;
}
function lhm_nospaces($text){
	return str_replace(" ", "", $$text);
}

function lmh_header_icons(){
	?>
		<div class="header-icon icon-bar show-hide-sidebar">
			<i class="icofont-navigation-menu"></i>
		</div>
		<div class="header-icon icon-search show-search-box">
			<i class="icofont-search"></i>
		</div>
	<?php
}
add_action('lmh_header_icons','lmh_header_icons',1);

/*
 * Search in Header
 */
function lmh_get_header_search(){
	lmh_header_search_form();
}

function lmh_header_search_form(){ ?>
	<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		<label>
			<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
			<div class="search-field"><input type="search" class="search-input" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" /></div>
		</label>
		<input type="submit" class="search-submit"
		value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
	</form><?php
}

add_action( 'main_navigation_bottom', 'lmh_get_header_search',10);