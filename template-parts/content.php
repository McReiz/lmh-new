<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package LMH_new
 */

do_action('lmhn_before_outer_post');
if ( is_singular() ) :
	do_action('lmhn_before_outer_post_singular');
	require get_template_directory() . '/template-parts/is-single.php';
	do_action('lmhn_after_outer_post_singular');
else:
	do_action('lmhn_before_outer_post_indivual');
	require get_template_directory() . '/template-parts/is-list.php';
	do_action('lmhn_after_outer_post_indivual');
endif;
do_action('lmhn_after_outer_post');
?>