<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package LMH_new
 */

get_header();
get_sidebar();
?>
	<?php do_action('lmh_single_outer_before_site_main') ?>
	<main id="primary" class="site-main site-single">
		<?php do_action('lmh_single_inner_before_site_main') ?>
		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation(
				array(
					'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'lmh-new' ) . '</span> <span class="nav-title">%title</span>',
					'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'lmh-new' ) . '</span> <span class="nav-title">%title</span>',
				)
			);

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
		<?php do_action('lmh_single_inner_after_site_main') ?>
	</main><!-- #main -->
	<?php do_action('lmh_single_outer_after_site_main') ?>	
<?php
get_footer();
