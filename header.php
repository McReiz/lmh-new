<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package LMH_new
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'lmh-new' ); ?></a>

	<div class="sticky-header-space"></div>
	<header id="masthead" class="site-header container">
		<div class="container-wrap">
			<div class="header-item site-branding">
				<?php
				the_custom_logo();
				if ( is_front_page() && is_home() ) :
					?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
				else :
					?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
				endif;
				$lmh_new_description = get_bloginfo( 'description', 'display' );
				if ( $lmh_new_description || is_customize_preview() ) :
					?>
					<p class="site-description"><?php echo $lmh_new_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
				<?php endif; ?>
			</div><!-- .site-branding -->
			<div class="header-item header-icons text-right">
				<?php do_action('lmh_header_icons') ?>
			</div>
			<div class="header-item main-navigation">
				<nav class="main-navigation__top">
					<?php
						$lmh_slider_control = '<div class="menu-slider-controls">';
						$lmh_slider_control .= '<div class="menu-slider-button-prev"><i class="icofont-curved-double-left"></i></div>';
						$lmh_slider_control .= '<div class="menu-slider-button-next"><i class="icofont-curved-double-right"></i></div>';
						$lmh_slider_control .= '</div>';

						wp_nav_menu(
							array(
								'theme_location'	=> 'menu-1',
								'menu_id'			=> 'primary-menu',
								'menu_class'		=> 'menu-slider-wrapper',
								'container_class'	=>	'menu-slider-container',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>'. $lmh_slider_control
							)
						);	
					?>
				</nav>
				<div class="main-navigation__bottom">
					<?php do_action('main_navigation_bottom') ?>
				</div>
			</div><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->

	<div id="main-body" class="container">
		<div class="container-wrap-full">