<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package LMH_new
 */

?>

			<footer id="main-footer">
				<div class="site-footer">
					<div class="footer__inner">
						<div class="site-footer-widget">
							<?php 
							if ( ! is_active_sidebar( 'footer-sidebar' ) ) {
								?> 
									<aside id="footer-sidebar" class="widget-area">
										<?php dynamic_sidebar( 'footer-sidebar' ); ?>
									</aside><!-- #secondary -->
								<?php
							}
							?>

							

						</div>
						<div class="site-footer-menu">
							
						</div>
						<div class="site-info">
							<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'lmh-new' ) ); ?>">
								<?php
								/* translators: %s: CMS name, i.e. WordPress. */
								printf( esc_html__( 'Proudly powered by %s', 'lmh-new' ), 'WordPress' );
								?>
							</a>
							<span class="sep"> | </span>
								<?php
								/* translators: 1: Theme name, 2: Theme author. */
								printf( esc_html__( 'Theme: %1$s by %2$s.', 'lmh-new' ), 'lmh-new', '<a href="http://underscores.me/">Underscores.me</a>' );
								?>
						</div><!-- .site-info -->
					</div>
				</div><!-- .site-footer -->
			</footer><!-- #main-footer -->
		</div><!-- .container-wrap -->
	</div><!-- .container -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
