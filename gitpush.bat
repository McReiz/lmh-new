@REM PUSH SENDING
@REM GET TIME
@FOR /f "tokens=2 delims==" %%G in ('wmic os get localdatetime /value') do @set datetime=%%G
@SET dd=%datetime:~6,2%
@SET mth=%datetime:~4,2%
@SET yyyy=%datetime:~0,4%
@SET Timestamp=%date:~3,2%%date:~0,2%%time:~0,2%%time:~3,2%%time:~6,2%
@SET CM=P%Timestamp%
@ECHO %CM%
@REM GIT
@git add .
@git commit -m "%CM%"
@git push origin master
@ECHO /////////////////////
@ECHO //// PUSH READY! ////
@ECHO /////////////////////