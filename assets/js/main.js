/**
 * main.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
( function($) {
	
	$(document).ready( function(){
		$('#sidebar .sidebar.widget-area, #main-footer .site-footer').stickySidebar({
		    topSpacing: 0,
		    bottomSpacing: 0
		});

		/* show and hidde sidebar */
		$('.show-hide-sidebar, .sidebar__close').click( function (){
			$('#sidebar').toggleClass('active');
		});

		$('.show-search-box').click( function (){
			$('.main-navigation__bottom').toggleClass('active');
		});

		var mySwiper = new Swiper('.menu-slider-container', {
			slidesPerView: 'auto',
			wrapperClass: 'menu-slider-wrapper',
			slideClass: 'menu-item',
	   	navigation: {
				nextEl: '.menu-slider-button-next',
				prevEl: '.menu-slider-button-prev'
			}
		});

		console.log(mySwiper);
	});
})(jQuery);
